# Manager backend project

The backend part of the TaskManager workflow.

#### Requisites to run
- JDK
- Maven
- PostgresSql server

#### How to run
- Clone the project
- Check and adjust the file `\src\main\resources\application.properties` for database configuration
- Create a empty database named `manager`
- On the project root folder, run `mvn spring-boot:run`
- The project will run on `http://localhost:8080`

##### Apiary
- You can check the full apiary in the link `https://leopoldomanager.docs.apiary.io`

##### Postman
- All API calls are available on the link `https://www.getpostman.com/collections/b1df46ad6854c1e8788c`
- Format: `Collection v2.1`

#### Next steps for evolution
- Authentication
- Better validation of required fields
- Implement a better CORS solution
- Analyze dependencies and remove unused
- Mock the Database for the tests
