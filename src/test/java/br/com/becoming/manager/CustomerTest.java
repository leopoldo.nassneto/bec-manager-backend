package br.com.becoming.manager;

import br.com.becoming.manager.model.Customer;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.HttpClientErrorException;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ManagerApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)

public class CustomerTest {

	@Autowired
	private TestRestTemplate restTemplate;

	private Customer customer;


	@Before
	public void setUp() {
		customer = new Customer();
		customer.setEmail("leopoldo.nassneto@gmail.com");
		customer.setName("Customer Leopoldo");
		customer.setAddress("Rua dos cravos");
		customer.setPhone("48-98812608");
	}

	@LocalServerPort
	private int port = 8080;

	private String getRootUrl() {
		return "http://localhost:" + port + "/api/v1/";
	}

	@Test
	public void testCreateUpdateDeleteValidCustomer() {
		// Create
		ResponseEntity<Customer> responseCustomer = restTemplate.postForEntity(getRootUrl() + "/customers", customer, Customer.class);
		Assert.assertNotNull(responseCustomer);
		Assert.assertNotNull(responseCustomer.getBody());
		Assert.assertEquals(HttpStatus.CREATED, responseCustomer.getStatusCode());

		int idCustomer = (int) responseCustomer.getBody().getId();

		// Update
		restTemplate.put(getRootUrl() + "/customer/" + idCustomer, this.customer);
		Customer updatedCustomer = restTemplate.getForObject(getRootUrl() + "/customer/" + idCustomer, Customer.class);
		Assert.assertNotNull(updatedCustomer);

		// Get By Id
		try {
			customer = restTemplate.getForObject(getRootUrl() + "/customer/" + idCustomer, Customer.class);
		} catch (final HttpClientErrorException e) {
			Assert.assertEquals(e.getStatusCode(), HttpStatus.OK);
		}

		// Delete
		Customer customerForDelete = restTemplate.getForObject(getRootUrl() + "/customer/" + idCustomer, Customer.class);
		Assert.assertNotNull(customerForDelete);
		restTemplate.delete(getRootUrl() + "/customer/" + idCustomer);
		try {
			customerForDelete = restTemplate.getForObject(getRootUrl() + "/customer/" + idCustomer, Customer.class);
		} catch (final HttpClientErrorException e) {
			Assert.assertEquals(e.getStatusCode(), HttpStatus.NOT_FOUND);
		}
	}

	@Test
	public void testGetAllCustomers() {
		HttpHeaders headers = new HttpHeaders();
		HttpEntity<String> entity = new HttpEntity<String>(null, headers);

		ResponseEntity<String> response = restTemplate.exchange(getRootUrl() + "/customers",
				HttpMethod.GET, entity, String.class);

		Assert.assertNotNull(response.getBody());
		Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
	}

	@Test
	public void testGetInvalidCustomerById() {
		Customer customer = restTemplate.getForObject(getRootUrl() + "/customers/0", Customer.class);
		System.out.println(customer.getName());
		Assert.assertNotNull(customer);
	}

	@Test
	public void testCreateCustomerWithoutFields() {
		Customer customer = new Customer();

		ResponseEntity<Customer> response = restTemplate.postForEntity(getRootUrl() + "/customers", customer, Customer.class);
		Assert.assertNotNull(response);
		Assert.assertNotNull(response.getBody());
		Assert.assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
	}

	@Test
	public void testCreateCustomerWithoutEmail() {
		Customer customer = new Customer();
		customer.setName("Leopoldo");
		customer.setAddress("Rua dos cravos");
		customer.setPhone("48-98812608");

		ResponseEntity<Customer> response = restTemplate.postForEntity(getRootUrl() + "/customers", customer, Customer.class);
		Assert.assertNotNull(response);
		Assert.assertNotNull(response.getBody());
		Assert.assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
	}

	@Test
	public void testCreateCustomerWithoutName() {
		Customer customer = new Customer();
		customer.setAddress("Rua dos cravos");
		customer.setEmail("leopoldo.nassneto@gmail.com");
		customer.setPhone("48-98812608");

		ResponseEntity<Customer> response = restTemplate.postForEntity(getRootUrl() + "/customers", customer, Customer.class);
		Assert.assertNotNull(response);
		Assert.assertNotNull(response.getBody());
		Assert.assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
	}

	@Test
	public void testCreateCustomerWithoutAddress() {
		Customer customer = new Customer();
		customer.setName("Leopoldo");
		customer.setEmail("leopoldo.nassneto@gmail.com");
		customer.setPhone("48-98812608");

		ResponseEntity<Customer> response = restTemplate.postForEntity(getRootUrl() + "/customers", customer, Customer.class);
		Assert.assertNotNull(response);
		Assert.assertNotNull(response.getBody());
		Assert.assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
	}

	@Test
	public void testCreateCustomerWithoutPhone() {
		Customer customer = new Customer();
		customer.setName("Leopoldo");
		customer.setAddress("Rua dos cravos");
		customer.setEmail("leopoldo.nassneto@gmail.com");

		ResponseEntity<Customer> response = restTemplate.postForEntity(getRootUrl() + "/customers", customer, Customer.class);
		Assert.assertNotNull(response);
		Assert.assertNotNull(response.getBody());
		Assert.assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
	}

	@Test
	public void testUpdateInvalidCustomer() {
		int id = 0;
		Customer customer = restTemplate.getForObject(getRootUrl() + "/customer/" + id, Customer.class);
		customer.setName("Leo(o)poldo");
		customer.setEmail("lnn@gmail.com");

		restTemplate.put(getRootUrl() + "/customer/" + id, customer);

		Customer updatedCustomer = restTemplate.getForObject(getRootUrl() + "/customer/" + id, Customer.class);
		Assert.assertNotNull(updatedCustomer);

		try {
			customer = restTemplate.getForObject(getRootUrl() + "/customer/" + id, Customer.class);
		} catch (final HttpClientErrorException e) {
			Assert.assertEquals(e.getStatusCode(), HttpStatus.OK);
		}
	}


	@Test
	public void testUpdateCustomerWithoutEmail() {
		int id = 10;
		Customer customer = restTemplate.getForObject(getRootUrl() + "/customer/" + id, Customer.class);
		customer.setName("Leo(o)poldo");
		customer.setAddress("Addr");
		customer.setPhone("484848");

		restTemplate.put(getRootUrl() + "/customer/" + id, customer);

		Customer updatedCustomer = restTemplate.getForObject(getRootUrl() + "/customer/" + id, Customer.class);
		Assert.assertNotNull(updatedCustomer);

		try {
			customer = restTemplate.getForObject(getRootUrl() + "/customer/" + id, Customer.class);
		} catch (final HttpClientErrorException e) {
			Assert.assertEquals(e.getStatusCode(), HttpStatus.OK);
		}
	}

	@Test
	public void testUpdateCustomerWithoutPhone() {
		int id = 10;
		Customer customer = restTemplate.getForObject(getRootUrl() + "/customer/" + id, Customer.class);
		customer.setName("Leo(o)poldo");
		customer.setEmail("lnn@gmail.com");
		customer.setAddress("Addr");

		restTemplate.put(getRootUrl() + "/customer/" + id, customer);

		Customer updatedCustomer = restTemplate.getForObject(getRootUrl() + "/customer/" + id, Customer.class);
		Assert.assertNotNull(updatedCustomer);

		try {
			customer = restTemplate.getForObject(getRootUrl() + "/customer/" + id, Customer.class);
		} catch (final HttpClientErrorException e) {
			Assert.assertEquals(e.getStatusCode(), HttpStatus.OK);
		}
	}

	@Test
	public void testUpdateCustomerWithoutName() {
		int id = 10;
		Customer customer = restTemplate.getForObject(getRootUrl() + "/customer/" + id, Customer.class);
		customer.setEmail("lnn@gmail.com");
		customer.setAddress("Addr");
		customer.setPhone("484848");

		restTemplate.put(getRootUrl() + "/customer/" + id, customer);

		Customer updatedCustomer = restTemplate.getForObject(getRootUrl() + "/customer/" + id, Customer.class);
		Assert.assertNotNull(updatedCustomer);

		try {
			customer = restTemplate.getForObject(getRootUrl() + "/customer/" + id, Customer.class);
		} catch (final HttpClientErrorException e) {
			Assert.assertEquals(e.getStatusCode(), HttpStatus.OK);
		}
	}

	@Test
	public void testUpdateCustomerWithoutAddress() {
		int id = 10;
		Customer customer = restTemplate.getForObject(getRootUrl() + "/customer/" + id, Customer.class);
		customer.setName("Leo(o)poldo");
		customer.setEmail("lnn@gmail.com");
		customer.setPhone("484848");

		restTemplate.put(getRootUrl() + "/customer/" + id, customer);

		Customer updatedCustomer = restTemplate.getForObject(getRootUrl() + "/customer/" + id, Customer.class);
		Assert.assertNotNull(updatedCustomer);

		try {
			customer = restTemplate.getForObject(getRootUrl() + "/customer/" + id, Customer.class);
		} catch (final HttpClientErrorException e) {
			Assert.assertEquals(e.getStatusCode(), HttpStatus.OK);
		}
	}

	@Test
	public void testDeleteInvalidCustomer() {
		int id = 0;
		Customer customer = restTemplate.getForObject(getRootUrl() + "/customer/" + id, Customer.class);
		Assert.assertNotNull(customer);

		restTemplate.delete(getRootUrl() + "/customer/" + id);

		try {
			customer = restTemplate.getForObject(getRootUrl() + "/customer/" + id, Customer.class);
		} catch (final HttpClientErrorException e) {
			Assert.assertEquals(e.getStatusCode(), HttpStatus.NOT_FOUND);
		}
	}

}