package br.com.becoming.manager;

import br.com.becoming.manager.model.User;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.HttpClientErrorException;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ManagerApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)

public class UserTest {

    @Autowired
    private TestRestTemplate restTemplate;

    private User user;


    @Before
    public void setUp() {
        user = new User();
        user.setEmail("leopoldo.nassneto@gmail.com");
        user.setName("User Leopoldo");
    }

    @LocalServerPort
    private int port = 8080;

    private String getRootUrl() {
        return "http://localhost:" + port + "/api/v1/";
    }

    @Test
    public void testCreateUpdateDeleteValidUser() {
        // Create
        ResponseEntity<User> responseUser = restTemplate.postForEntity(getRootUrl() + "/users", user, User.class);
        Assert.assertNotNull(responseUser);
        Assert.assertNotNull(responseUser.getBody());
        Assert.assertEquals(HttpStatus.CREATED, responseUser.getStatusCode());
        int idUser = (int) responseUser.getBody().getId();

        // Update
        restTemplate.put(getRootUrl() + "/user/" + idUser, this.user);

        // GetById
        User updatedUser = restTemplate.getForObject(getRootUrl() + "/user/" + idUser, User.class);
        Assert.assertNotNull(updatedUser);

        // Delete
        User userForDelete = restTemplate.getForObject(getRootUrl() + "/user/" + idUser, User.class);
        Assert.assertNotNull(userForDelete);
        restTemplate.delete(getRootUrl() + "/user/" + idUser);

        try {
            userForDelete = restTemplate.getForObject(getRootUrl() + "/user/" + idUser, User.class);
        } catch (final HttpClientErrorException e) {
            Assert.assertEquals(e.getStatusCode(), HttpStatus.NOT_FOUND);
        }
    }

    @Test
    public void testGetAllUsers() {
        HttpHeaders headers = new HttpHeaders();
        HttpEntity<String> entity = new HttpEntity<String>(null, headers);

        ResponseEntity<String> response = restTemplate.exchange(getRootUrl() + "/users",
                HttpMethod.GET, entity, String.class);

        Assert.assertNotNull(response.getBody());
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void testGetInvalidUserById() {
        User user = restTemplate.getForObject(getRootUrl() + "/user/0", User.class);
        System.out.println(user.getName());
        Assert.assertNotNull(user);
    }

    @Test
    public void testCreateUserWithoutFields() {
        User user = new User();

        ResponseEntity<User> response = restTemplate.postForEntity(getRootUrl() + "/users", user, User.class);
        Assert.assertNotNull(response);
        Assert.assertNotNull(response.getBody());
        Assert.assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @Test
    public void testCreateUserWithoutEmail() {
        User user = new User();
        user.setName("Leopoldo");

        ResponseEntity<User> response = restTemplate.postForEntity(getRootUrl() + "/users", user, User.class);
        Assert.assertNotNull(response);
        Assert.assertNotNull(response.getBody());
        Assert.assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @Test
    public void testCreateUserWithoutName() {
        User user = new User();
        user.setEmail("leopoldo.nassneto@gmail.com");

        ResponseEntity<User> response = restTemplate.postForEntity(getRootUrl() + "/users", user, User.class);
        Assert.assertNotNull(response);
        Assert.assertNotNull(response.getBody());
        Assert.assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @Test
    public void testUpdateInvalidUser() {
        int id = 0;
        User user = restTemplate.getForObject(getRootUrl() + "/user/" + id, User.class);
        user.setName("Leo(o)poldo");
        user.setEmail("lnn@gmail.com");

        restTemplate.put(getRootUrl() + "/user/" + id, user);

        User updatedUser = restTemplate.getForObject(getRootUrl() + "/user/" + id, User.class);
        Assert.assertNotNull(updatedUser);

        try {
            user = restTemplate.getForObject(getRootUrl() + "/user/" + id, User.class);
        } catch (final HttpClientErrorException e) {
            Assert.assertEquals(e.getStatusCode(), HttpStatus.OK);
        }
    }


    @Test
    public void testUpdateUserWithoutEmail() {
        int id = 10;
        User user = restTemplate.getForObject(getRootUrl() + "/user/" + id, User.class);
        user.setName("Leo(o)poldo");

        restTemplate.put(getRootUrl() + "/user/" + id, user);

        User updatedUser = restTemplate.getForObject(getRootUrl() + "/user/" + id, User.class);
        Assert.assertNotNull(updatedUser);

        try {
            user = restTemplate.getForObject(getRootUrl() + "/user/" + id, User.class);
        } catch (final HttpClientErrorException e) {
            Assert.assertEquals(e.getStatusCode(), HttpStatus.OK);
        }
    }


    @Test
    public void testUpdateUserWithoutName() {
        int id = 10;
        User user = restTemplate.getForObject(getRootUrl() + "/user/" + id, User.class);
        user.setEmail("lnn@gmail.com");

        restTemplate.put(getRootUrl() + "/user/" + id, user);

        User updatedUser = restTemplate.getForObject(getRootUrl() + "/user/" + id, User.class);
        Assert.assertNotNull(updatedUser);

        try {
            user = restTemplate.getForObject(getRootUrl() + "/user/" + id, User.class);
        } catch (final HttpClientErrorException e) {
            Assert.assertEquals(e.getStatusCode(), HttpStatus.OK);
        }
    }

    @Test
    public void testDeleteInvalidUser() {
        int id = 0;
        User user = restTemplate.getForObject(getRootUrl() + "/user/" + id, User.class);
        Assert.assertNotNull(user);

        restTemplate.delete(getRootUrl() + "/user/" + id);

        try {
            user = restTemplate.getForObject(getRootUrl() + "/user/" + id, User.class);
        } catch (final HttpClientErrorException e) {
            Assert.assertEquals(e.getStatusCode(), HttpStatus.NOT_FOUND);
        }
    }

}