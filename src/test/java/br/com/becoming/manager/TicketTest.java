package br.com.becoming.manager;

import br.com.becoming.manager.model.Customer;
import br.com.becoming.manager.model.Ticket;
import br.com.becoming.manager.model.User;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.HttpClientErrorException;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ManagerApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)

public class TicketTest {

    @Autowired
    private TestRestTemplate restTemplate;

    private Ticket ticket;
    private Customer customer;
    private User user;

    @Before
    public void setUp() {
        customer = new Customer();
        customer.setId(1);
        customer.setEmail("leopoldo.nassneto@gmail.com");
        customer.setName("Ticket Leopoldo");
        customer.setAddress("Rua dos cravos");
        customer.setPhone("48-98812608");

        user = new User();
        user.setId(1);
        user.setEmail("leopoldo.nassneto@gmail.com");
        user.setName("Ticket Leopoldo");

        ticket = new Ticket();
        ticket.setStatus(1);
        ticket.setCustomer(this.customer);
        ticket.setEquipment("Ticket Equipm");
        ticket.setEquipmentBrand("Lenovo");
        ticket.setEquipmentKind("Notebook");
        ticket.setIssue("nao liga");
        ticket.setTechnical(this.user);
    }

    @LocalServerPort
    private int port = 8080;

    private String getRootUrl() {
        return "http://localhost:" + port + "/api/v1/";
    }

    @Test
    public void testCreateUpdateDeleteValidTicket() {
        // Create User
        ResponseEntity<User> responseUser = restTemplate.postForEntity(getRootUrl() + "/users", user, User.class);
        Assert.assertNotNull(responseUser);
        Assert.assertNotNull(responseUser.getBody());
        Assert.assertEquals(HttpStatus.CREATED, responseUser.getStatusCode());

        int idUser = (int) responseUser.getBody().getId();
        this.user.setId(idUser);

        // Create Customer
        ResponseEntity<Customer> responseCustomer = restTemplate.postForEntity(getRootUrl() + "/customers", customer, Customer.class);
        Assert.assertNotNull(responseCustomer);
        Assert.assertNotNull(responseCustomer.getBody());
        Assert.assertEquals(HttpStatus.CREATED, responseCustomer.getStatusCode());

        int idCustomer = (int) responseCustomer.getBody().getId();
        this.customer.setId(idCustomer);

        this.ticket.setTechnical(this.user);
        this.ticket.setCustomer(this.customer);

        // Create Ticket
        ResponseEntity<Ticket> response = restTemplate.postForEntity(getRootUrl() + "/tickets", this.ticket, Ticket.class);
        Assert.assertNotNull(response);
        Assert.assertNotNull(response.getBody());
        Assert.assertEquals(HttpStatus.CREATED, response.getStatusCode());

        int id = (int) response.getBody().getId();

        // Update Ticket
        Ticket ticket = restTemplate.getForObject(getRootUrl() + "/ticket/" + id, Ticket.class);
        restTemplate.put(getRootUrl() + "/ticket/" + id, this.ticket);

        // Get By Id
        Ticket updatedTicket = restTemplate.getForObject(getRootUrl() + "/ticket/" + id, Ticket.class);
        Assert.assertNotNull(updatedTicket);

        // Delete Ticket
        Ticket ticketForDelete = restTemplate.getForObject(getRootUrl() + "/ticket/" + id, Ticket.class);
        Assert.assertNotNull(ticketForDelete);
        restTemplate.delete(getRootUrl() + "/ticket/" + id);

        try {
            ticketForDelete = restTemplate.getForObject(getRootUrl() + "/ticket/" + id, Ticket.class);
        } catch (final HttpClientErrorException e) {
            Assert.assertEquals(e.getStatusCode(), HttpStatus.NOT_FOUND);
        }

        // Delete User
        User userForDelete = restTemplate.getForObject(getRootUrl() + "/user/" + idUser, User.class);
        Assert.assertNotNull(userForDelete);

        restTemplate.delete(getRootUrl() + "/user/" + idUser);

        try {
            userForDelete = restTemplate.getForObject(getRootUrl() + "/user/" + idUser, User.class);
        } catch (final HttpClientErrorException e) {
            Assert.assertEquals(e.getStatusCode(), HttpStatus.NOT_FOUND);
        }


        // Customer User
        Customer customerForDelete = restTemplate.getForObject(getRootUrl() + "/customer/" + idCustomer, Customer.class);
        Assert.assertNotNull(customerForDelete);

        restTemplate.delete(getRootUrl() + "/customer/" + idCustomer);

        try {
            customerForDelete = restTemplate.getForObject(getRootUrl() + "/customer/" + idCustomer, Customer.class);
        } catch (final HttpClientErrorException e) {
            Assert.assertEquals(e.getStatusCode(), HttpStatus.NOT_FOUND);
        }
    }

    @Test
    public void testGetAllTickets() {
        HttpHeaders headers = new HttpHeaders();
        HttpEntity<String> entity = new HttpEntity<String>(null, headers);

        ResponseEntity<String> response = restTemplate.exchange(getRootUrl() + "/tickets",
                HttpMethod.GET, entity, String.class);

        Assert.assertNotNull(response.getBody());
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void testGetAllTicketsWithStatus() {
        HttpHeaders headers = new HttpHeaders();
        HttpEntity<String> entity = new HttpEntity<String>(null, headers);

        ResponseEntity<String> response = restTemplate.exchange(getRootUrl() + "/tickets?status=1",
                HttpMethod.GET, entity, String.class);

        Assert.assertNotNull(response.getBody());
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void testGetAllTicketsWithTechnical() {
        HttpHeaders headers = new HttpHeaders();
        HttpEntity<String> entity = new HttpEntity<String>(null, headers);

        ResponseEntity<String> response = restTemplate.exchange(getRootUrl() + "/tickets?technical=1",
                HttpMethod.GET, entity, String.class);

        Assert.assertNotNull(response.getBody());
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void testGetAllTicketsWithStatusAndTechical() {
        HttpHeaders headers = new HttpHeaders();
        HttpEntity<String> entity = new HttpEntity<String>(null, headers);

        ResponseEntity<String> response = restTemplate.exchange(getRootUrl() + "/tickets?status=1&technical=1",
                HttpMethod.GET, entity, String.class);

        Assert.assertNotNull(response.getBody());
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
    }



    @Test
    public void testGetInvalidTicketById() {
        Ticket ticket = restTemplate.getForObject(getRootUrl() + "/ticket/0", Ticket.class);
        Assert.assertNotNull(ticket);
    }

    @Test
    public void testCreateTicketWithoutFields() {
        Ticket ticket = new Ticket();

        ResponseEntity<Ticket> response = restTemplate.postForEntity(getRootUrl() + "/tickets", ticket, Ticket.class);
        Assert.assertNotNull(response);
        Assert.assertNotNull(response.getBody());
        Assert.assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @Test
    public void testCreateTicketWithoutStatus() {
        ticket = new Ticket();
        ticket.setCustomer(this.customer);
        ticket.setEquipment("Equipm");
        ticket.setEquipmentBrand("Lenovo");
        ticket.setEquipmentKind("Notebook");
        ticket.setIssue("nao liga");
        ticket.setTechnical(this.user);
        ticket.setStatus(0);

        ResponseEntity<Ticket> response = restTemplate.postForEntity(getRootUrl() + "/tickets", ticket, Ticket.class);
        Assert.assertNotNull(response);
        Assert.assertNotNull(response.getBody());
        Assert.assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }


    @Test
    public void testCreateTicketWithoutCustomer() {
        ticket = new Ticket();
        ticket.setStatus(1);
        ticket.setEquipment("Equipm");
        ticket.setEquipmentBrand("Lenovo");
        ticket.setEquipmentKind("Notebook");
        ticket.setIssue("nao liga");
        ticket.setTechnical(this.user);

        ResponseEntity<Ticket> response = restTemplate.postForEntity(getRootUrl() + "/tickets", ticket, Ticket.class);
        Assert.assertNotNull(response);
        Assert.assertNotNull(response.getBody());
        Assert.assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }


    @Test
    public void testCreateTicketWithoutEquipment() {
        ticket = new Ticket();
        ticket.setStatus(1);
        ticket.setCustomer(this.customer);
        ticket.setEquipmentBrand("Lenovo");
        ticket.setEquipmentKind("Notebook");
        ticket.setIssue("nao liga");
        ticket.setTechnical(this.user);

        ResponseEntity<Ticket> response = restTemplate.postForEntity(getRootUrl() + "/tickets", ticket, Ticket.class);
        Assert.assertNotNull(response);
        Assert.assertNotNull(response.getBody());
        Assert.assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }


    @Test
    public void testCreateTicketWithoutBrand() {
        ticket = new Ticket();
        ticket.setStatus(1);
        ticket.setCustomer(this.customer);
        ticket.setEquipment("Equipm");
        ticket.setEquipmentKind("Notebook");
        ticket.setIssue("nao liga");
        ticket.setTechnical(this.user);

        ResponseEntity<Ticket> response = restTemplate.postForEntity(getRootUrl() + "/tickets", ticket, Ticket.class);
        Assert.assertNotNull(response);
        Assert.assertNotNull(response.getBody());
        Assert.assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @Test
    public void testCreateTicketWithoutEquipmentKind() {
        ticket = new Ticket();
        ticket.setStatus(1);
        ticket.setCustomer(this.customer);
        ticket.setEquipment("Equipm");
        ticket.setEquipmentBrand("Lenovo");
        ticket.setIssue("nao liga");
        ticket.setTechnical(this.user);

        ResponseEntity<Ticket> response = restTemplate.postForEntity(getRootUrl() + "/tickets", ticket, Ticket.class);
        Assert.assertNotNull(response);
        Assert.assertNotNull(response.getBody());
        Assert.assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }


    @Test
    public void testCreateTicketWithoutIssue() {
        ticket = new Ticket();
        ticket.setStatus(1);
        ticket.setCustomer(this.customer);
        ticket.setEquipment("Equipm");
        ticket.setEquipmentBrand("Lenovo");
        ticket.setEquipmentKind("Notebook");
        ticket.setTechnical(this.user);

        ResponseEntity<Ticket> response = restTemplate.postForEntity(getRootUrl() + "/tickets", ticket, Ticket.class);
        Assert.assertNotNull(response);
        Assert.assertNotNull(response.getBody());
        Assert.assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @Test
    public void testCreateTicketWithoutTechnical() {
        ticket = new Ticket();
        ticket.setStatus(1);
        ticket.setCustomer(this.customer);
        ticket.setEquipment("Equipm");
        ticket.setEquipmentBrand("Lenovo");
        ticket.setEquipmentKind("Notebook");
        ticket.setIssue("nao liga");

        ResponseEntity<Ticket> response = restTemplate.postForEntity(getRootUrl() + "/tickets", ticket, Ticket.class);
        Assert.assertNotNull(response);
        Assert.assertNotNull(response.getBody());
        Assert.assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @Test
    public void testUpdateInvalidTicket() {
        int id = 0;
        Ticket ticket = restTemplate.getForObject(getRootUrl() + "/ticket/" + id, Ticket.class);

        ticket.setStatus(1);
        ticket.setCustomer(this.customer);
        ticket.setEquipment("Equipm");
        ticket.setEquipmentBrand("Lenovo");
        ticket.setEquipmentKind("Notebook");
        ticket.setIssue("nao liga");
        ticket.setTechnical(this.user);

        restTemplate.put(getRootUrl() + "/ticket/" + id, ticket);

        Ticket updatedTicket = restTemplate.getForObject(getRootUrl() + "/ticket/" + id, Ticket.class);
        Assert.assertNotNull(updatedTicket);

        try {
            ticket = restTemplate.getForObject(getRootUrl() + "/ticket/" + id, Ticket.class);
        } catch (final HttpClientErrorException e) {
            Assert.assertEquals(e.getStatusCode(), HttpStatus.OK);
        }
    }


    @Test
    public void testUpdateTicketWithoutStatus() {
        int id = 10;
        Ticket ticket = restTemplate.getForObject(getRootUrl() + "/ticket/" + id, Ticket.class);

        ticket.setCustomer(this.customer);
        ticket.setEquipment("Equipm");
        ticket.setEquipmentBrand("Lenovo");
        ticket.setEquipmentKind("Notebook");
        ticket.setIssue("nao liga");
        ticket.setTechnical(this.user);
        ticket.setStatus(0);

        restTemplate.put(getRootUrl() + "/ticket/" + id, ticket);

        Ticket updatedTicket = restTemplate.getForObject(getRootUrl() + "/ticket/" + id, Ticket.class);
        Assert.assertNotNull(updatedTicket);

        try {
            ticket = restTemplate.getForObject(getRootUrl() + "/ticket/" + id, Ticket.class);
        } catch (final HttpClientErrorException e) {
            Assert.assertEquals(e.getStatusCode(), HttpStatus.OK);
        }
    }


    @Test
    public void testUpdateTicketWithoutCustomer() {
        int id = 10;
        Ticket ticket = restTemplate.getForObject(getRootUrl() + "/ticket/" + id, Ticket.class);

        ticket.setStatus(1);
        ticket.setEquipment("Equipm");
        ticket.setEquipmentBrand("Lenovo");
        ticket.setEquipmentKind("Notebook");
        ticket.setIssue("nao liga");
        ticket.setTechnical(this.user);

        restTemplate.put(getRootUrl() + "/ticket/" + id, ticket);

        Ticket updatedTicket = restTemplate.getForObject(getRootUrl() + "/ticket/" + id, Ticket.class);
        Assert.assertNotNull(updatedTicket);

        try {
            ticket = restTemplate.getForObject(getRootUrl() + "/ticket/" + id, Ticket.class);
        } catch (final HttpClientErrorException e) {
            Assert.assertEquals(e.getStatusCode(), HttpStatus.OK);
        }
    }


    @Test
    public void testUpdateTicketWithoutEquipment() {
        int id = 10;
        Ticket ticket = restTemplate.getForObject(getRootUrl() + "/ticket/" + id, Ticket.class);

        ticket.setStatus(1);
        ticket.setCustomer(this.customer);
        ticket.setEquipmentBrand("Lenovo");
        ticket.setEquipmentKind("Notebook");
        ticket.setIssue("nao liga");
        ticket.setTechnical(this.user);

        restTemplate.put(getRootUrl() + "/ticket/" + id, ticket);

        Ticket updatedTicket = restTemplate.getForObject(getRootUrl() + "/ticket/" + id, Ticket.class);
        Assert.assertNotNull(updatedTicket);

        try {
            ticket = restTemplate.getForObject(getRootUrl() + "/ticket/" + id, Ticket.class);
        } catch (final HttpClientErrorException e) {
            Assert.assertEquals(e.getStatusCode(), HttpStatus.OK);
        }
    }

    @Test
    public void testUpdateTicketWithoutEquipmentBrand() {
        int id = 10;
        Ticket ticket = restTemplate.getForObject(getRootUrl() + "/ticket/" + id, Ticket.class);

        ticket.setStatus(1);
        ticket.setCustomer(this.customer);
        ticket.setEquipment("Equipm");
        ticket.setEquipmentKind("Notebook");
        ticket.setIssue("nao liga");
        ticket.setTechnical(this.user);

        restTemplate.put(getRootUrl() + "/ticket/" + id, ticket);

        Ticket updatedTicket = restTemplate.getForObject(getRootUrl() + "/ticket/" + id, Ticket.class);
        Assert.assertNotNull(updatedTicket);

        try {
            ticket = restTemplate.getForObject(getRootUrl() + "/ticket/" + id, Ticket.class);
        } catch (final HttpClientErrorException e) {
            Assert.assertEquals(e.getStatusCode(), HttpStatus.OK);
        }
    }


    @Test
    public void testUpdateTicketWithoutEquipmentKind() {
        int id = 10;
        Ticket ticket = restTemplate.getForObject(getRootUrl() + "/ticket/" + id, Ticket.class);

        ticket.setStatus(1);
        ticket.setCustomer(this.customer);
        ticket.setEquipment("Equipm");
        ticket.setEquipmentBrand("Lenovo");
        ticket.setIssue("nao liga");
        ticket.setTechnical(this.user);

        restTemplate.put(getRootUrl() + "/ticket/" + id, ticket);

        Ticket updatedTicket = restTemplate.getForObject(getRootUrl() + "/ticket/" + id, Ticket.class);
        Assert.assertNotNull(updatedTicket);

        try {
            ticket = restTemplate.getForObject(getRootUrl() + "/ticket/" + id, Ticket.class);
        } catch (final HttpClientErrorException e) {
            Assert.assertEquals(e.getStatusCode(), HttpStatus.OK);
        }
    }


    @Test
    public void testUpdateTicketWithoutIssue() {
        int id = 10;
        Ticket ticket = restTemplate.getForObject(getRootUrl() + "/ticket/" + id, Ticket.class);

        ticket.setStatus(1);
        ticket.setCustomer(this.customer);
        ticket.setEquipment("Equipm");
        ticket.setEquipmentBrand("Lenovo");
        ticket.setEquipmentKind("Notebook");
        ticket.setTechnical(this.user);

        restTemplate.put(getRootUrl() + "/ticket/" + id, ticket);

        Ticket updatedTicket = restTemplate.getForObject(getRootUrl() + "/ticket/" + id, Ticket.class);
        Assert.assertNotNull(updatedTicket);

        try {
            ticket = restTemplate.getForObject(getRootUrl() + "/ticket/" + id, Ticket.class);
        } catch (final HttpClientErrorException e) {
            Assert.assertEquals(e.getStatusCode(), HttpStatus.OK);
        }
    }


    @Test
    public void testUpdateTicketWithoutTechnical() {
        int id = 10;
        Ticket ticket = restTemplate.getForObject(getRootUrl() + "/ticket/" + id, Ticket.class);

        ticket.setStatus(1);
        ticket.setCustomer(this.customer);
        ticket.setEquipment("Equipm");
        ticket.setEquipmentBrand("Lenovo");
        ticket.setEquipmentKind("Notebook");
        ticket.setIssue("nao liga");

        restTemplate.put(getRootUrl() + "/ticket/" + id, ticket);

        Ticket updatedTicket = restTemplate.getForObject(getRootUrl() + "/ticket/" + id, Ticket.class);
        Assert.assertNotNull(updatedTicket);

        try {
            ticket = restTemplate.getForObject(getRootUrl() + "/ticket/" + id, Ticket.class);
        } catch (final HttpClientErrorException e) {
            Assert.assertEquals(e.getStatusCode(), HttpStatus.OK);
        }
    }


    @Test
    public void testDeleteInvalidTicket() {
        int id = 0;
        Ticket ticket = restTemplate.getForObject(getRootUrl() + "/ticket/" + id, Ticket.class);
        Assert.assertNotNull(ticket);

        restTemplate.delete(getRootUrl() + "/ticket/" + id);

        try {
            ticket = restTemplate.getForObject(getRootUrl() + "/ticket/" + id, Ticket.class);
        } catch (final HttpClientErrorException e) {
            Assert.assertEquals(e.getStatusCode(), HttpStatus.NOT_FOUND);
        }
    }

}