package br.com.becoming.manager;

import br.com.becoming.manager.model.Customer;
import br.com.becoming.manager.model.Ticket;
import br.com.becoming.manager.model.TicketHistory;
import br.com.becoming.manager.model.User;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.HttpClientErrorException;

import java.text.ParseException;
import java.text.SimpleDateFormat;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ManagerApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)

public class TicketHistoryTest {

    @Autowired
    private TestRestTemplate restTemplate;

    private TicketHistory ticketHistory;
    private Ticket ticket;
    private User user;
    private Customer customer;

    @Before
    public void setUp() throws ParseException {
        customer = new Customer();
        customer.setId(1);
        customer.setEmail("leopoldo.nassneto@gmail.com");
        customer.setName("TicketHistory Leopoldo");
        customer.setAddress("Rua dos cravos");
        customer.setPhone("48-98812608");

        user = new User();
        user.setId(1);
        user.setEmail("leopoldo.nassneto@gmail.com");
        user.setName("TicketHistory Leopoldo");

        ticket = new Ticket();
        ticket.setId(1);
        ticket.setStatus(1);
        ticket.setCustomer(this.customer);
        ticket.setEquipment("TicketHistory Equipm");
        ticket.setEquipmentBrand("Lenovo");
        ticket.setEquipmentKind("Notebook");
        ticket.setIssue("nao liga");
        ticket.setTechnical(this.user);

        ticketHistory = new TicketHistory();
        ticketHistory.setTicket(this.ticket);
        ticketHistory.setDescription("Trabalhando");
        ticketHistory.setKind(1);
        SimpleDateFormat FormatStartedAt = new SimpleDateFormat("dd/MM/yyyy");
        ticketHistory.setStartedAtDate(FormatStartedAt.parse("23/11/2019"));
        ticketHistory.setStartedAtTime("08:00:00");
        ticketHistory.setEndedAtDate(FormatStartedAt.parse("23/12/2019"));
        ticketHistory.setEndedAtTime("12:00:00");
        ticketHistory.setTechnical(this.user);
    }

    @LocalServerPort
    private int port = 8080;

    private String getRootUrl() {
        return "http://localhost:" + port + "/api/v1/";
    }

    @Test
    public void testCreateUpdateDeleteValidTicketHistory() {
        // Create User
        ResponseEntity<User> responseUser = restTemplate.postForEntity(getRootUrl() + "/users", user, User.class);
        Assert.assertNotNull(responseUser);
        Assert.assertNotNull(responseUser.getBody());
        Assert.assertEquals(HttpStatus.CREATED, responseUser.getStatusCode());

        int idUser = (int) responseUser.getBody().getId();
        this.user.setId(idUser);

        // Create Customer
        ResponseEntity<Customer> responseCustomer = restTemplate.postForEntity(getRootUrl() + "/customers", customer, Customer.class);
        Assert.assertNotNull(responseCustomer);
        Assert.assertNotNull(responseCustomer.getBody());
        Assert.assertEquals(HttpStatus.CREATED, responseCustomer.getStatusCode());

        int idCustomer = (int) responseCustomer.getBody().getId();
        this.customer.setId(idCustomer);

        this.ticket.setTechnical(this.user);
        this.ticket.setCustomer(this.customer);

        // Create Ticket
        ResponseEntity<Ticket> response = restTemplate.postForEntity(getRootUrl() + "/tickets", this.ticket, Ticket.class);
        Assert.assertNotNull(response);
        Assert.assertNotNull(response.getBody());
        Assert.assertEquals(HttpStatus.CREATED, response.getStatusCode());

        int id = (int) response.getBody().getId();


        this.ticket.setId(id);
        this.ticketHistory.setTicket(this.ticket);
        this.ticketHistory.setTechnical(this.user);

        // Create Ticket History
        ResponseEntity<TicketHistory> responseTicketHistory = restTemplate.postForEntity(getRootUrl() + "/tickets_history", this.ticketHistory, TicketHistory.class);
        Assert.assertNotNull(responseTicketHistory);
        Assert.assertNotNull(responseTicketHistory.getBody());
        Assert.assertEquals(HttpStatus.CREATED, responseTicketHistory.getStatusCode());

        int idTicketHistory = (int) responseTicketHistory.getBody().getId();

        // Update TicketHistory
        TicketHistory ticketHistory = restTemplate.getForObject(getRootUrl() + "/ticket_history/" + idTicketHistory, TicketHistory.class);
        restTemplate.put(getRootUrl() + "/ticket_history/" + idTicketHistory, this.ticketHistory);

        // Get By id TicketHistory
        TicketHistory updatedTicketHistory = restTemplate.getForObject(getRootUrl() + "/ticket_history/" + idTicketHistory, TicketHistory.class);
        Assert.assertNotNull(updatedTicketHistory);

        // Delete TicketHistory
        TicketHistory ticketHistoryForDelete = restTemplate.getForObject(getRootUrl() + "/ticket_history/" + idTicketHistory, TicketHistory.class);
        Assert.assertNotNull(ticketHistoryForDelete);
        restTemplate.delete(getRootUrl() + "/ticket_history/" + idTicketHistory);
        try {
            ticketHistoryForDelete = restTemplate.getForObject(getRootUrl() + "/ticket/" + id, TicketHistory.class);
        } catch (final HttpClientErrorException e) {
            Assert.assertEquals(e.getStatusCode(), HttpStatus.NOT_FOUND);
        }







        // Delete Ticket
        Ticket ticketForDelete = restTemplate.getForObject(getRootUrl() + "/ticket/" + id, Ticket.class);
        Assert.assertNotNull(ticketForDelete);
        restTemplate.delete(getRootUrl() + "/ticket/" + id);

        try {
            ticketForDelete = restTemplate.getForObject(getRootUrl() + "/ticket/" + id, Ticket.class);
        } catch (final HttpClientErrorException e) {
            Assert.assertEquals(e.getStatusCode(), HttpStatus.NOT_FOUND);
        }

        // Delete User
        User userForDelete = restTemplate.getForObject(getRootUrl() + "/user/" + idUser, User.class);
        Assert.assertNotNull(userForDelete);

        restTemplate.delete(getRootUrl() + "/user/" + idUser);

        try {
            userForDelete = restTemplate.getForObject(getRootUrl() + "/user/" + idUser, User.class);
        } catch (final HttpClientErrorException e) {
            Assert.assertEquals(e.getStatusCode(), HttpStatus.NOT_FOUND);
        }


        // Customer User
        Customer customerForDelete = restTemplate.getForObject(getRootUrl() + "/customer/" + idCustomer, Customer.class);
        Assert.assertNotNull(customerForDelete);

        restTemplate.delete(getRootUrl() + "/customer/" + idCustomer);

        try {
            customerForDelete = restTemplate.getForObject(getRootUrl() + "/customer/" + idCustomer, Customer.class);
        } catch (final HttpClientErrorException e) {
            Assert.assertEquals(e.getStatusCode(), HttpStatus.NOT_FOUND);
        }
    }

    @Test
    public void testGetAllTicketsHistory() {
        HttpHeaders headers = new HttpHeaders();
        HttpEntity<String> entity = new HttpEntity<String>(null, headers);

        ResponseEntity<String> response = restTemplate.exchange(getRootUrl() + "/tickets_history",
                HttpMethod.GET, entity, String.class);

        Assert.assertNotNull(response.getBody());
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
    }


    @Test
    public void testGetAllTicketsHistoryWithTicket() {
        HttpHeaders headers = new HttpHeaders();
        HttpEntity<String> entity = new HttpEntity<String>(null, headers);

        ResponseEntity<String> response = restTemplate.exchange(getRootUrl() + "/tickets_history?ticket=1",
                HttpMethod.GET, entity, String.class);

        Assert.assertNotNull(response.getBody());
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void testGetInvalidTicketHistoryById() {
        TicketHistory ticketHistory = restTemplate.getForObject(getRootUrl() + "/ticket_history/0", TicketHistory.class);
        Assert.assertNotNull(ticketHistory);
    }

    @Test
    public void testCreateTicketHistoryWithoutFields() {
        TicketHistory ticketHistory = new TicketHistory();

        ResponseEntity<TicketHistory> response = restTemplate.postForEntity(getRootUrl() + "/tickets_history", ticketHistory, TicketHistory.class);
        Assert.assertNotNull(response);
        Assert.assertNotNull(response.getBody());
        Assert.assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @Test
    public void testCreateTicketWithoutTicket() throws ParseException {
        ticketHistory = new TicketHistory();
        ticketHistory.setDescription("Trabalhando");
        ticketHistory.setKind(1);
        SimpleDateFormat FormatStartedAt = new SimpleDateFormat("dd/MM/yyyy");
        ticketHistory.setStartedAtDate(FormatStartedAt.parse("23/11/2019"));
        ticketHistory.setStartedAtTime("08:00:00");
        ticketHistory.setEndedAtDate(FormatStartedAt.parse("23/12/2019"));
        ticketHistory.setEndedAtTime("12:00:00");
        ticketHistory.setTechnical(this.user);

        ResponseEntity<TicketHistory> response = restTemplate.postForEntity(getRootUrl() + "/tickets_history", ticketHistory, TicketHistory.class);
        Assert.assertNotNull(response);
        Assert.assertNotNull(response.getBody());
        Assert.assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }


    @Test
    public void testCreateTicketWithoutDescription() throws ParseException {
        ticketHistory = new TicketHistory();
        ticketHistory.setTicket(this.ticket);
        ticketHistory.setKind(1);
        SimpleDateFormat FormatStartedAt = new SimpleDateFormat("dd/MM/yyyy");
        ticketHistory.setStartedAtDate(FormatStartedAt.parse("23/11/2019"));
        ticketHistory.setStartedAtTime("08:00:00");
        ticketHistory.setEndedAtDate(FormatStartedAt.parse("23/12/2019"));
        ticketHistory.setEndedAtTime("12:00:00");
        ticketHistory.setTechnical(this.user);

        ResponseEntity<TicketHistory> response = restTemplate.postForEntity(getRootUrl() + "/tickets_history", ticketHistory, TicketHistory.class);
        Assert.assertNotNull(response);
        Assert.assertNotNull(response.getBody());
        Assert.assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }


    @Test
    public void testCreateTicketWithoutKind() throws ParseException {
        ticketHistory = new TicketHistory();
        ticketHistory.setTicket(this.ticket);
        ticketHistory.setDescription("Trabalhando");
        SimpleDateFormat FormatStartedAt = new SimpleDateFormat("dd/MM/yyyy");
        ticketHistory.setStartedAtDate(FormatStartedAt.parse("23/11/2019"));
        ticketHistory.setStartedAtTime("08:00:00");
        ticketHistory.setEndedAtDate(FormatStartedAt.parse("23/12/2019"));
        ticketHistory.setEndedAtTime("12:00:00");
        ticketHistory.setTechnical(this.user);

        ResponseEntity<TicketHistory> response = restTemplate.postForEntity(getRootUrl() + "/tickets_history", ticketHistory, TicketHistory.class);
        Assert.assertNotNull(response);
        Assert.assertNotNull(response.getBody());
        Assert.assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }


    @Test
    public void testCreateTicketWithoutTechnical() throws ParseException {
        ticketHistory = new TicketHistory();
        ticketHistory.setTicket(this.ticket);
        ticketHistory.setDescription("Trabalhando");
        ticketHistory.setKind(1);
        SimpleDateFormat FormatStartedAt = new SimpleDateFormat("dd/MM/yyyy");
        ticketHistory.setStartedAtDate(FormatStartedAt.parse("23/11/2019"));
        ticketHistory.setStartedAtTime("08:00:00");
        ticketHistory.setEndedAtDate(FormatStartedAt.parse("23/12/2019"));
        ticketHistory.setEndedAtTime("12:00:00");

        ResponseEntity<TicketHistory> response = restTemplate.postForEntity(getRootUrl() + "/tickets_history", ticketHistory, TicketHistory.class);
        Assert.assertNotNull(response);
        Assert.assertNotNull(response.getBody());
        Assert.assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @Test
    public void testUpdateInvalidTicketHistory() throws ParseException {
        int id = 0;
        TicketHistory ticketHistory = restTemplate.getForObject(getRootUrl() + "/ticketHistory/" + id, TicketHistory.class);
        ticketHistory.setTicket(this.ticket);
        ticketHistory.setDescription("Trabalhando");
        ticketHistory.setKind(1);
        SimpleDateFormat FormatStartedAt = new SimpleDateFormat("dd/MM/yyyy");
        ticketHistory.setStartedAtDate(FormatStartedAt.parse("23/11/2019"));
        ticketHistory.setStartedAtTime("08:00:00");
        ticketHistory.setEndedAtDate(FormatStartedAt.parse("23/12/2019"));
        ticketHistory.setEndedAtTime("12:00:00");
        ticketHistory.setTechnical(this.user);

        restTemplate.put(getRootUrl() + "/ticket_history/" + id, ticketHistory);

        TicketHistory updatedTicketHistory = restTemplate.getForObject(getRootUrl() + "/ticket_history/" + id, TicketHistory.class);
        Assert.assertNotNull(updatedTicketHistory);

        try {
            ticketHistory = restTemplate.getForObject(getRootUrl() + "/ticket_history/" + id, TicketHistory.class);
        } catch (final HttpClientErrorException e) {
            Assert.assertEquals(e.getStatusCode(), HttpStatus.OK);
        }
    }


    @Test
    public void testUpdateTicketHistoryWithoutTicket() throws ParseException {
        int id = 10;
        TicketHistory ticketHistory = restTemplate.getForObject(getRootUrl() + "/ticketHistory/" + id, TicketHistory.class);
        ticketHistory.setDescription("Trabalhando");
        ticketHistory.setKind(1);
        SimpleDateFormat FormatStartedAt = new SimpleDateFormat("dd/MM/yyyy");
        ticketHistory.setStartedAtDate(FormatStartedAt.parse("23/11/2019"));
        ticketHistory.setStartedAtTime("08:00:00");
        ticketHistory.setEndedAtDate(FormatStartedAt.parse("23/12/2019"));
        ticketHistory.setEndedAtTime("12:00:00");
        ticketHistory.setTechnical(this.user);

        restTemplate.put(getRootUrl() + "/ticket_history/" + id, ticketHistory);

        TicketHistory updatedTicketHistory = restTemplate.getForObject(getRootUrl() + "/ticket_history/" + id, TicketHistory.class);
        Assert.assertNotNull(updatedTicketHistory);

        try {
            ticketHistory = restTemplate.getForObject(getRootUrl() + "/ticket_history/" + id, TicketHistory.class);
        } catch (final HttpClientErrorException e) {
            Assert.assertEquals(e.getStatusCode(), HttpStatus.OK);
        }
    }

    @Test
    public void testUpdateTicketHistoryWithoutDescription() throws ParseException {
        int id = 10;
        TicketHistory ticketHistory = restTemplate.getForObject(getRootUrl() + "/ticketHistory/" + id, TicketHistory.class);

        ticketHistory.setTicket(this.ticket);
        ticketHistory.setKind(1);
        SimpleDateFormat FormatStartedAt = new SimpleDateFormat("dd/MM/yyyy");
        ticketHistory.setStartedAtDate(FormatStartedAt.parse("23/11/2019"));
        ticketHistory.setStartedAtTime("08:00:00");
        ticketHistory.setEndedAtDate(FormatStartedAt.parse("23/12/2019"));
        ticketHistory.setEndedAtTime("12:00:00");
        ticketHistory.setTechnical(this.user);

        restTemplate.put(getRootUrl() + "/ticket_history/" + id, ticketHistory);

        TicketHistory updatedTicketHistory = restTemplate.getForObject(getRootUrl() + "/ticket_history/" + id, TicketHistory.class);
        Assert.assertNotNull(updatedTicketHistory);

        try {
            ticketHistory = restTemplate.getForObject(getRootUrl() + "/ticket_history/" + id, TicketHistory.class);
        } catch (final HttpClientErrorException e) {
            Assert.assertEquals(e.getStatusCode(), HttpStatus.OK);
        }
    }

    @Test
    public void testUpdateTicketHistoryWithoutKind() throws ParseException {
        int id = 10;
        TicketHistory ticketHistory = restTemplate.getForObject(getRootUrl() + "/ticketHistory/" + id, TicketHistory.class);

        ticketHistory.setTicket(this.ticket);
        ticketHistory.setDescription("Trabalhando");
        SimpleDateFormat FormatStartedAt = new SimpleDateFormat("dd/MM/yyyy");
        ticketHistory.setStartedAtDate(FormatStartedAt.parse("23/11/2019"));
        ticketHistory.setStartedAtTime("08:00:00");
        ticketHistory.setEndedAtDate(FormatStartedAt.parse("23/12/2019"));
        ticketHistory.setEndedAtTime("12:00:00");
        ticketHistory.setTechnical(this.user);

        restTemplate.put(getRootUrl() + "/ticket_history/" + id, ticketHistory);

        TicketHistory updatedTicketHistory = restTemplate.getForObject(getRootUrl() + "/ticket_history/" + id, TicketHistory.class);
        Assert.assertNotNull(updatedTicketHistory);

        try {
            ticketHistory = restTemplate.getForObject(getRootUrl() + "/ticket_history/" + id, TicketHistory.class);
        } catch (final HttpClientErrorException e) {
            Assert.assertEquals(e.getStatusCode(), HttpStatus.OK);
        }
    }

    @Test
    public void testUpdateTicketHistoryWithoutTechnical() throws ParseException {
        int id = 1;
        TicketHistory ticketHistory = restTemplate.getForObject(getRootUrl() + "/ticketHistory/" + id, TicketHistory.class);

        ticketHistory.setTicket(this.ticket);
        ticketHistory.setDescription("Trabalhando");
        ticketHistory.setKind(1);
        SimpleDateFormat FormatStartedAt = new SimpleDateFormat("dd/MM/yyyy");
        ticketHistory.setStartedAtDate(FormatStartedAt.parse("23/11/2019"));
        ticketHistory.setStartedAtTime("08:00:00");
        ticketHistory.setEndedAtDate(FormatStartedAt.parse("23/12/2019"));
        ticketHistory.setEndedAtTime("12:00:00");

        restTemplate.put(getRootUrl() + "/ticket_history/" + id, ticketHistory);

        TicketHistory updatedTicketHistory = restTemplate.getForObject(getRootUrl() + "/ticket_history/" + id, TicketHistory.class);
        Assert.assertNotNull(updatedTicketHistory);

        try {
            ticketHistory = restTemplate.getForObject(getRootUrl() + "/ticket_history/" + id, TicketHistory.class);
        } catch (final HttpClientErrorException e) {
            Assert.assertEquals(e.getStatusCode(), HttpStatus.OK);
        }
    }


    @Test
    public void testDeleteInvalidTicketHistory() {
        int id = 0;
        TicketHistory ticketHistory = restTemplate.getForObject(getRootUrl() + "/ticket_history/" + id, TicketHistory.class);
        Assert.assertNotNull(ticketHistory);

        restTemplate.delete(getRootUrl() + "/ticket_history/" + id);

        try {
            ticketHistory = restTemplate.getForObject(getRootUrl() + "/ticket_history/" + id, TicketHistory.class);
        } catch (final HttpClientErrorException e) {
            Assert.assertEquals(e.getStatusCode(), HttpStatus.NOT_FOUND);
        }
    }

}