package br.com.becoming.manager.controller;

import br.com.becoming.manager.exception.RequiredFieldException;
import br.com.becoming.manager.exception.ResourceNotFoundException;
import br.com.becoming.manager.model.TicketHistory;
import br.com.becoming.manager.repository.TicketHistoryRepository;
import br.com.becoming.manager.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/v1")
public class TicketHistoryController {
    @Autowired
    private TicketHistoryRepository ticketHistoryRepository;
    private UserRepository userRepository;


    /**
     * Get all users list
     *
     * @return the tickets history list
     */
    @GetMapping("/tickets_history")
    public List<TicketHistory> getAllTicketsHistory(@RequestParam(value = "ticket", required = false, defaultValue = "-1") long ticketId)
            throws ResourceNotFoundException {
        if (ticketId == -1) {
            return ticketHistoryRepository.findAll(new Sort(Sort.Direction.DESC, "id"));
        } else {
            return ticketHistoryRepository.findAllByTicket(ticketId);
        }
    }

    /**
     * Create ticket history
     *
     * @param ticketHistory the ticket history
     * @return the ticket history
     */
    @PostMapping("/tickets_history")
    @ResponseStatus( HttpStatus.CREATED )
    public TicketHistory createTicketHistory(@Valid @RequestBody TicketHistory ticketHistory) {

        if ((ticketHistory.getDescription() == null) || (ticketHistory.getDescription().length()<1)) {
            throw new RequiredFieldException("description");
        }
        if ((ticketHistory.getTicket() == null) || (ticketHistory.getTicket().getId() == 0)) {
            throw new RequiredFieldException("ticket");
        }
        if ((ticketHistory.getTechnical() == null) || (ticketHistory.getTechnical().getId() == 0)) {
            throw new RequiredFieldException("technical");
        }

        if (ticketHistory.getKind() == 0) {
            throw new RequiredFieldException("kind");
        }

        ticketHistory.setKind(ticketHistory.getKind());

        return ticketHistoryRepository.save(ticketHistory);
    }

    @GetMapping("/ticket_history/{id}")
    public ResponseEntity<TicketHistory> getTicketsHistoryById(@PathVariable(value = "id") Long ticketHistoryId)
            throws ResourceNotFoundException {
        TicketHistory ticketHistory =
                ticketHistoryRepository
                        .findById(ticketHistoryId)
                        .orElseThrow(() -> new ResourceNotFoundException("Ticket History not found [" + ticketHistoryId+"]"));
        return ResponseEntity.ok().body(ticketHistory);
    }

    /**
     * Update ticket history response entity.
     *
     * @param ticketHistoryId the ticket history id
     * @param ticketHistoryDetails the ticket history details
     * @return the response entity
     * @throws ResourceNotFoundException the resource not found exception
     */
    @PutMapping("/ticket_history/{id}")
    public ResponseEntity<TicketHistory> updateTicketHistory(
            @PathVariable(value = "id") Long ticketHistoryId, @Valid @RequestBody TicketHistory ticketHistoryDetails)
            throws ResourceNotFoundException, RequiredFieldException {

        if (ticketHistoryDetails.getDescription() == null) {
            throw new RequiredFieldException("description");
        }

        if (ticketHistoryDetails.getKind() == 0) {
            throw new RequiredFieldException("kind");
        }

        if ((ticketHistoryDetails.getTicket() == null) || (ticketHistoryDetails.getTicket().getId() == 0)) {
            throw new RequiredFieldException("ticket");
        }

        if ((ticketHistoryDetails.getTechnical() == null) || (ticketHistoryDetails.getTechnical().getId() == 0)) {
            throw new RequiredFieldException("technical");
        }

        TicketHistory ticketHistory =
                ticketHistoryRepository
                        .findById(ticketHistoryId)
                        .orElseThrow(() -> new ResourceNotFoundException("Ticket History not found [" + ticketHistoryId+"]"));

        ticketHistory.setDescription(ticketHistoryDetails.getDescription());
        ticketHistory.setKind(ticketHistoryDetails.getKind());
        ticketHistory.setTicket(ticketHistoryDetails.getTicket());
        ticketHistory.setTechnical(ticketHistoryDetails.getTechnical());

        if (ticketHistoryDetails.getEndedAtDate() != null) {
            ticketHistory.setEndedAtDate(ticketHistoryDetails.getEndedAtDate());
        }

        if (ticketHistoryDetails.getEndedAtTime() != null) {
            ticketHistory.setEndedAtTime(ticketHistoryDetails.getEndedAtTime());
        }

        if (ticketHistoryDetails.getStartedAtDate() != null) {
            ticketHistory.setStartedAtDate(ticketHistoryDetails.getStartedAtDate());
        }

        if (ticketHistoryDetails.getStartedAtTime() != null) {
            ticketHistory.setStartedAtTime(ticketHistoryDetails.getStartedAtTime());
        }

        final TicketHistory updatedTicketHistory = ticketHistoryRepository.save(ticketHistory);
        return ResponseEntity.ok(updatedTicketHistory);
    }


    /**
     * Delete TicketHistory map
     *
     * @param ticketHistoryId the ticket history id
     * @return the map
     * @throws ResourceNotFoundException the exception
     */
    @DeleteMapping("/ticket_history/{id}")
    public Map<String, Boolean> deleteTicketHistory(@PathVariable(value = "id") Long ticketHistoryId) throws ResourceNotFoundException {
        TicketHistory ticketHistory =
                ticketHistoryRepository
                        .findById(ticketHistoryId)
                        .orElseThrow(() -> new ResourceNotFoundException("Ticket History not found ["+ticketHistoryId+"]"));
        ticketHistoryRepository.delete(ticketHistory);
        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }
}