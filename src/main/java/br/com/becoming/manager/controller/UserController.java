package br.com.becoming.manager.controller;

import br.com.becoming.manager.exception.RequiredFieldException;
import br.com.becoming.manager.exception.ResourceNotFoundException;
import br.com.becoming.manager.model.User;
import br.com.becoming.manager.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/v1")
public class UserController {
    @Autowired
    private UserRepository userRepository;

    /**
     * Get all users list
     *
     * @return the users list
     */
    @GetMapping("/users")
    public List<User> getAllUsers() {
        return userRepository.findAll(new Sort(Sort.Direction.ASC, "name"));
    }

    /**
     * Create user
     *
     * @param user the user
     * @return the user
     */
    @PostMapping("/users")
    @ResponseStatus( HttpStatus.CREATED )
    public User createUser(@Valid @RequestBody User user) {

        if ((user.getEmail() == null) || (user.getEmail().length()<1)) {
            throw new RequiredFieldException("email");
        }
        if ((user.getName() == null) || (user.getName().length()<1)) {
            throw new RequiredFieldException("name");
        }
        return userRepository.save(user);
    }

    /**
     * Gets users by id.
     *
     * @param userId the user id
     * @return the users by id
     * @throws ResourceNotFoundException the resource not found exception
     */
    @GetMapping("/user/{id}")
    public ResponseEntity<User> getUsersById(@PathVariable(value = "id") Long userId)
            throws ResourceNotFoundException {
        User user =
                userRepository
                        .findById(userId)
                        .orElseThrow(() -> new ResourceNotFoundException("User not found [" + userId+"]"));
        return ResponseEntity.ok().body(user);
    }

    /**
     * Update user response entity.
     *
     * @param userId the user id
     * @param userDetails the user details
     * @return the response entity
     * @throws ResourceNotFoundException the resource not found exception
     */
    @PutMapping("/user/{id}")
    public ResponseEntity<User> updateUser(
            @PathVariable(value = "id") Long userId, @Valid @RequestBody User userDetails)
            throws ResourceNotFoundException {

        if ((userDetails.getEmail() == null) || (userDetails.getEmail().length() == 0)) {
            throw new RequiredFieldException("email");
        }

        if ((userDetails.getName() == null)  || (userDetails.getName().length() == 0)) {
            throw new RequiredFieldException("Name");
        }

        User user =
                userRepository
                        .findById(userId)
                        .orElseThrow(() -> new ResourceNotFoundException("User not found [" + userId+"]"));
        user.setEmail(userDetails.getEmail());
        user.setName(userDetails.getName());

        final User updatedUser = userRepository.save(user);
        return ResponseEntity.ok(updatedUser);
    }
    /**
     * Delete User map
     *
     * @param userId the user id
     * @return the map
     * @throws ResourceNotFoundException the exception
     */
    @DeleteMapping("/user/{id}")
    public Map<String, Boolean> deleteUser(@PathVariable(value = "id") Long userId) throws ResourceNotFoundException {
        User user =
                userRepository
                        .findById(userId)
                        .orElseThrow(() -> new ResourceNotFoundException("User not found ["+userId+"]"));
        userRepository.delete(user);
        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }
}