package br.com.becoming.manager.controller;

import br.com.becoming.manager.exception.ResourceNotFoundException;
import br.com.becoming.manager.exception.RequiredFieldException;
import br.com.becoming.manager.model.Customer;
import br.com.becoming.manager.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/v1")
public class CustomerController {
    @Autowired
    private CustomerRepository customerRepository;

    /**
     * Get all customers list
     *
     * @return the customer lsist
     */
    @GetMapping("/customers")
    public List<Customer> getAllCustomers() {
        return customerRepository.findAll(new Sort(Sort.Direction.ASC, "name"));
    }

    /**
     * Gets customers by id.
     *
     * @param customerId the customer id
     * @return the customers by id
     * @throws ResourceNotFoundException the resource not found exception
     */
    @CrossOrigin(origins = "*" )
    @GetMapping("/customer/{id}")
    public ResponseEntity<Customer> getCustomersById(@PathVariable(value = "id") Long customerId)
            throws ResourceNotFoundException {
        Customer customer =
                customerRepository
                        .findById(customerId)
                        .orElseThrow(() -> new ResourceNotFoundException("Customer not found [" + customerId+"]"));
        return ResponseEntity.ok().body(customer);
    }


    /**
     * Create customer
     *
     * @param customer the customer
     * @return the customer
     */
    @PostMapping("/customers")
    @ResponseStatus( HttpStatus.CREATED )
    public Customer createCustomer(@Valid @RequestBody Customer customer) {
        if ((customer.getEmail() == null) || (customer.getEmail().length()<1)) {
            throw new RequiredFieldException("email");
        }
        if ((customer.getAddress() == null)|| (customer.getAddress().length()<1)) {
            throw new RequiredFieldException("address");
        }
        if ((customer.getName() == null) || (customer.getName().length()<1)){
            throw new RequiredFieldException("name");
        }
        if ((customer.getPhone() == null)|| (customer.getPhone().length()<1)) {
            throw new RequiredFieldException("phone");
        }

        return customerRepository.save(customer);
    }


    /**
     * Update customer response entity.
     *
     * @param customerId the customer id
     * @param customerDetails the customer details
     * @return the response entity
     * @throws ResourceNotFoundException the resource not found exception
     */
    @PutMapping("/customer/{id}")
    public ResponseEntity<Customer> updateCustomer(
            @PathVariable(value = "id") Long customerId, @Valid @RequestBody Customer customerDetails)
            throws ResourceNotFoundException {

        if ((customerDetails.getEmail() == null) || (customerDetails.getEmail().length() == 0)){
            throw new RequiredFieldException("Email");
        }

        if ((customerDetails.getName() == null) || (customerDetails.getName().length() == 0)){
            throw new RequiredFieldException("Name");
        }

        if ((customerDetails.getAddress() == null) || (customerDetails.getAddress().length() == 0)){
            throw new RequiredFieldException("Address");
        }

        if ((customerDetails.getPhone() == null) || (customerDetails.getPhone().length() == 0)){
            throw new RequiredFieldException("Phone");
        }

        Customer customer =
                customerRepository
                        .findById(customerId)
                        .orElseThrow(() -> new ResourceNotFoundException("Customer not found [" + customerId+"]"));
        customer.setEmail(customerDetails.getEmail());
        customer.setName(customerDetails.getName());
        customer.setAddress(customerDetails.getAddress());
        customer.setPhone(customerDetails.getPhone());

        final Customer updatedCustomer = customerRepository.save(customer);
        return ResponseEntity.ok(updatedCustomer);
    }

    /**
     * Delete Customer map
     *
     * @param customerId the customer id
     * @return the map
     * @throws ResourceNotFoundException the exception
     */
    @DeleteMapping("/customer/{id}")
    public Map<String, Boolean> deleteCustomer(@PathVariable(value = "id") Long customerId) throws ResourceNotFoundException {
        Customer customer =
                customerRepository
                        .findById(customerId)
                        .orElseThrow(() -> new ResourceNotFoundException("Customer not found ["+customerId+"]"));
        customerRepository.delete(customer);
        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }
}