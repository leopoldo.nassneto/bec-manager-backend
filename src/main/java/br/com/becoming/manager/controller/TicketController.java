package br.com.becoming.manager.controller;

import br.com.becoming.manager.exception.RequiredFieldException;
import br.com.becoming.manager.exception.ResourceNotFoundException;
import br.com.becoming.manager.model.Ticket;
import br.com.becoming.manager.repository.TicketRepository;
import br.com.becoming.manager.repository.CustomerRepository;
import br.com.becoming.manager.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/v1")
public class TicketController {
    @Autowired
    private TicketRepository ticketRepository;
    private CustomerRepository customerRepository;
    private UserRepository userRepository;
    /**
     * Get all Ticket list
     *
     * @return the Tickets list
     */
    @GetMapping("/tickets")
    public List<Ticket> getAllTickets(@RequestParam(value = "status", required = false, defaultValue = "-1") int status,
                                      @RequestParam(value = "technical", required = false, defaultValue = "-1") int technical) {
        if ((status != -1) || (technical != -1) ){
            if (status == -1) {
                return ticketRepository.findAllByTechnical(technical);
            } else if (technical == -1) {
                return ticketRepository.findAllByStatus(status);
            } else {
                return ticketRepository.findAllByStatusAndTechnical(status, technical);
            }

        }
        return ticketRepository.findAll(new Sort(Sort.Direction.ASC, "createdAt"));
    }

    /**
     * Create ticket
     *
     * @param ticket the ticket
     * @return the ticket
     */
    @PostMapping("/tickets")
    @ResponseStatus( HttpStatus.CREATED )
    public Ticket createTicket(@Valid @RequestBody Ticket ticket)
            throws ResourceNotFoundException {

        if ((ticket.getIssue() == null) || (ticket.getIssue().length() == 0)) {
            throw new RequiredFieldException("issue");
        }

        if ((ticket.getEquipment() == null) || (ticket.getEquipment().length() == 0)) {
            throw new RequiredFieldException("Equipment");
        }

        if ((ticket.getEquipmentBrand() == null) || (ticket.getEquipmentBrand().length() == 0)) {
            throw new RequiredFieldException("Equipment Brand");
        }

        if ((ticket.getEquipmentKind() == null) || (ticket.getEquipmentKind().length() == 0)) {
            throw new RequiredFieldException("Equipment Kind");
        }

        if ((ticket.getStatus() == 0)) {
            throw new RequiredFieldException("Status");
        }

        if ((ticket.getCustomer() == null) || (ticket.getCustomer().getId() == 0)) {
            throw new RequiredFieldException("Customer");
        }

        if ((ticket.getTechnical() == null) || (ticket.getTechnical().getId() == 0)) {
            throw new RequiredFieldException("Technical");
        }

        return ticketRepository.save(ticket);
    }

    /**
     * Gets Tickets by id.
     *
     * @param ticketId the ticket id
     * @return the tickets by id
     * @throws ResourceNotFoundException the resource not found exception
     */
    @GetMapping("/ticket/{id}")
    public ResponseEntity<Ticket> getTicketsById(@PathVariable(value = "id") Long ticketId)
            throws ResourceNotFoundException {
        Ticket ticket =
                ticketRepository
                        .findById(ticketId)
                        .orElseThrow(() -> new ResourceNotFoundException("Ticket not found [" + ticketId+"]"));
        return ResponseEntity.ok().body(ticket);
    }

    /**
     * Update ticket response entity.
     *
     * @param ticketId the ticket id
     * @param ticketDetails the ticket details
     * @return the response entity
     * @throws ResourceNotFoundException the resource not found exception
     */
    @PutMapping("/ticket/{id}")
    public ResponseEntity<Ticket> updateTicket(
            @PathVariable(value = "id") Long ticketId, @Valid @RequestBody Ticket ticketDetails)
            throws ResourceNotFoundException {

        if ((ticketDetails.getIssue() == null) || (ticketDetails.getIssue().length() == 0)) {
            throw new RequiredFieldException("Issue");
        }

        if ((ticketDetails.getEquipmentBrand() == null) || (ticketDetails.getEquipmentBrand().length() == 0)) {
            throw new RequiredFieldException("Equipment Brand");
        }

        if ((ticketDetails.getEquipmentKind() == null) || (ticketDetails.getEquipmentKind().length() == 0)) {
            throw new RequiredFieldException("Equipment Kind");
        }

        if ((ticketDetails.getEquipment() == null) || (ticketDetails.getEquipment().length() == 0)) {
            throw new RequiredFieldException("Equipment");
        }

        if (ticketDetails.getCustomer() == null || (ticketDetails.getCustomer().getId() == 0)) {
            throw new RequiredFieldException("Customer");
        }

        if ((ticketDetails.getTechnical() == null) || (ticketDetails.getTechnical().getId() == 0)) {
            throw new RequiredFieldException("Technical");
        }

        if (ticketDetails.getStatus() == 0) {
            throw new RequiredFieldException("Status");
        }

        Ticket ticket =
                ticketRepository
                        .findById(ticketId)
                        .orElseThrow(() -> new ResourceNotFoundException("Ticket not found [" + ticketId+"]"));

        ticket.setIssue(ticketDetails.getIssue());
        ticket.setCustomer(ticketDetails.getCustomer());
        ticket.setTechnical(ticketDetails.getTechnical());
        ticket.setStatus(ticketDetails.getStatus());
        ticket.setEquipment(ticketDetails.getEquipment());
        ticket.setEquipmentKind(ticketDetails.getEquipmentKind());
        ticket.setEquipmentBrand(ticketDetails.getEquipmentBrand());

        final Ticket updatedTicket = ticketRepository.save(ticket);
        return ResponseEntity.ok(updatedTicket);
    }

    /**
     * Delete Ticket map
     *
     * @param ticketId the ticket id
     * @return the map
     * @throws ResourceNotFoundException the exception
     */
    @DeleteMapping("/ticket/{id}")
    public Map<String, Boolean> deleteTicket(@PathVariable(value = "id") Long ticketId) throws ResourceNotFoundException {
        Ticket ticket =
                ticketRepository
                        .findById(ticketId)
                        .orElseThrow(() -> new ResourceNotFoundException("Ticket not found ["+ticketId+"]"));
        ticketRepository.delete(ticket);
        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }
}