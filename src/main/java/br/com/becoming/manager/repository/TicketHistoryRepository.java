package br.com.becoming.manager.repository;

import br.com.becoming.manager.model.TicketHistory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TicketHistoryRepository extends JpaRepository<TicketHistory, Long> {

    @Query("select t from TicketHistory t where t.ticket.id = :ticketId order by id desc")
    List<TicketHistory> findAllByTicket(Long ticketId);
}