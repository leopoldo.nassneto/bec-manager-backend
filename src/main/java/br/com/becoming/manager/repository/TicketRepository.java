package br.com.becoming.manager.repository;

import br.com.becoming.manager.model.Ticket;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TicketRepository extends JpaRepository<Ticket, Long> {
    @Query("select t from Ticket t where t.status = :status order by createdAt asc")
    List<Ticket> findAllByStatus(int status);
    @Query("select t from Ticket t where t.technical.id = :technical order by createdAt asc")
    List<Ticket> findAllByTechnical(long technical);
    @Query("select t from Ticket t where t.status = :status and t.technical.id = :technical order by createdAt asc")
    List<Ticket> findAllByStatusAndTechnical(int status, long technical);
}