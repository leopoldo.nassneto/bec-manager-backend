package br.com.becoming.manager.model;

import javax.persistence.*;
import org.hibernate.annotations.CreationTimestamp;
import java.util.Date;

@Entity
@Table(name = "users")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "email", nullable = false)
    private String email;

    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_at", nullable = false)
    private Date createdAt;

    /**
     * Get the user id
     *
     * @return the user id
     */
    public long getId()   {
        return id;
    }
    /**
     * Set the user id
     *
     * @param id the user id
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * Get the user name
     *
     * @return the user name
     */
    public String getName()  {
        return name;
    }

    /**
     * Set the user name
     *
     * @param name the user name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Get the user email
     *
     * @return the user email
     */
    public String getEmail()  {
        return email;
    }

    /**
     * Set the user email
     *
     * @param email the user email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Gets created at.
     *
     * @return the created at
     */
    public Date getCreatedAt()  {
        return createdAt;
    }

    /**
     * Sets created at.
     *
     * @param createdAt the created at
     */
    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

}