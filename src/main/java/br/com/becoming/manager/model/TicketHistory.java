package br.com.becoming.manager.model;

import javax.persistence.*;
import org.hibernate.annotations.CreationTimestamp;

import java.util.Date;

@Entity
@Table(name = "tickets_history")
public class TicketHistory {

    public static final int KIND_COMMENT = 1;
    public static final int KIND_WORK = 2;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "description", nullable = false)
    private String description;

    @ManyToOne
    @JoinColumn(name = "ticket_id")
    private Ticket ticket;

    @Column(name = "kind", nullable = false)
    private int kind;

    @ManyToOne
    @JoinColumn(name = "technical_id")
    private User technical;

    @Basic
    @Temporal(TemporalType.DATE)
    @Column(name = "started_at_date")
    private Date startedAtDate;

    @Column(name = "started_at_time")
    private String startedAtTime;

    @Basic
    @Temporal(TemporalType.DATE)
    @Column(name = "ended_at_date")
    private Date endedAtDate;

    @Column(name = "ended_at_time")
    private String endedAtTime;

    /**
     * Get the ticket history id
     *
     * @return the ticket history id
     */
    public long getId() {
        return id;
    }

    /**
     * Set the ticket history id
     *
     * @param id the ticket history id
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * Get the ticket history description
     *
     * @return the ticket history description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Set the ticket history description
     *
     * @param description the ticket history description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Get the ticket
     *
     * @return the ticket
     */
    public Ticket getTicket() {
        return ticket;
    }

    /**
     * Set the ticket
     *
     * @param ticket the ticket
     */
    public void setTicket(Ticket ticket)  {
        this.ticket = ticket;
    }

    /**
     * Get the technical
     *
     * @return the technical
     */
    public User getTechnical() {
        return technical;
    }

    /**
     * Set the technical
     *
     * @param technical the technical
     */
    public void setTechnical(User technical) {
        this.technical = technical;
    }

    /**
     * Get the ticket history kind
     *
     * @return the ticket history kind
     */
    public int getKind() {
        return kind;
    }

    /**
     * Set the ticket history kind
     *
     * @param kind the kind
     */
    public void setKind(int kind)  {
        this.kind = kind;
    }

    /**
     * Gets started at.
     *
     * @return the started at
     */
    public Date getStartedAtDate()  {
        return startedAtDate;
    }

    /**
     * Sets started at.
     *
     * @param startedAtDate the started at
     */
    public void setStartedAtDate(Date startedAtDate)  {
        this.startedAtDate = startedAtDate;
    }

    /**
     * Gets started at.
     *
     * @return the started at
     */
    public String  getStartedAtTime()  {
        return startedAtTime;
    }

    /**
     * Sets started at.
     *
     * @param startedAtTime the started at
     */
    public void setStartedAtTime(String startedAtTime)  {
        this.startedAtTime = startedAtTime;
    }


    /**
     * Gets ended at
     *
     * @return the ended at
     */
    public Date getEndedAtDate()  {
        return endedAtDate;
    }

    /**
     * Sets ended at
     *
     * @param endedAtDate the ended at
     */
    public void setEndedAtDate(Date endedAtDate)  {
        this.endedAtDate = endedAtDate;
    }

    /**
     * Gets ended at
     *
     * @return the ended at
     */
    public String  getEndedAtTime()  {
        return endedAtTime;
    }

    /**
     * Sets ended at
     *
     * @param endedAtTime the ended at
     */
    public void setEndedAtTime(String endedAtTime)  {
        this.endedAtTime = endedAtTime;
    }


}