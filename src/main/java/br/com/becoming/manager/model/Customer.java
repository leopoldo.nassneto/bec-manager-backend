package br.com.becoming.manager.model;

import javax.persistence.*;
import org.hibernate.annotations.CreationTimestamp;
import java.util.Date;

@Entity
@Table(name = "customers")
public class Customer {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "email", nullable = false)
    private String email;

    @Column(name = "address", nullable = false)
    private String address;

    @Column(name = "phone", nullable = false)
    private String phone;

    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_at", nullable = false)
    private Date createdAt;

    /**
    * Get the custommer id
    *
    * @return the customer id
    */
    public long getId() {
    return id;
    }

    /**
     * Set the customer id
     *
     * @param id the id
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * Get the customer name
     *
     * @return the customer name
     */
    public String getName() {
        return name;
    }

    /**
     * Set the customer name
     *
     * @param name the customer name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Get the customer email
     *
     * @return the customer email
     */
    public String getEmail() {
        return email;
    }

    /**
     * Set the customer email
     *
     * @param email the customer email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Get the customer phone
     *
     * @return the customer phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * Set the customer phone
     *
     * @param phone the customer phone
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * Get the customer address
     *
     * @return the customer address
     */
    public String getAddress() {
        return address;
    }

    /**
     * Set the customer address
     *
     * @param address the customer address
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * Gets created at.
     *
     * @return the created at
     */
    public Date getCreatedAt()  {
        return createdAt;
    }

    /**
     * Sets created at.
     *
     * @param createdAt the created at
     */
    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

}