package br.com.becoming.manager.model;

import javax.persistence.*;
import org.hibernate.annotations.CreationTimestamp;

import java.util.Date;

@Entity
@Table(name = "tickets")
public class Ticket {

    public static final int STATUS_QUEUED = 1;
    public static final int STATUS_DOING = 2;
    public static final int STATUS_BLOCKED = 3;
    public static final int STATUS_DONE = 4;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "equipment", nullable = false)
    private String equipment;

    @Column(name = "equipment_brand", nullable = false)
    private String equipmentBrand;

    @Column(name = "equipment_kind", nullable = false)
    private String equipmentKind;

    @Column(name = "issue", nullable = false)
    private String issue;

    @Column(name = "status", nullable = false)
    private int status = 1;

    @ManyToOne
    @JoinColumn(name = "customer_id")
    private Customer customer;

    @ManyToOne
    @JoinColumn(name = "technical_id")
    private User technical;

    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_at", nullable = false)
    private Date createdAt;

    /**
     * Get the ticket id
     *
     * @return the ticket id
     */
    public long getId() {
        return id;
    }

    /**
     * Set the ticket id
     *
     * @param id the id
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * Get the ticket equipment
     *
     * @return the ticket equipment
     */
    public String getEquipment() {
        return equipment;
    }

    /**
     * Set the ticket equipment
     *
     * @param equipment the ticket equipmentnt
     */
    public void setEquipment(String equipment) {
        this.equipment = equipment;
    }

    /**
     * Get the Equipment Brand
     *
     * @return the Equipment Brand
     */
    public String getEquipmentBrand() {
        return equipmentBrand;
    }

    /**
     * Set the Equipment Brand
     *
     * @param equipmentBrand the Equipment Brand
     */
    public void setEquipmentBrand(String equipmentBrand) {
        this.equipmentBrand = equipmentBrand;
    }

    /**
     * Get the Equipment Kind
     *
     * @return the Equipment Kind
     */
    public String getEquipmentKind() {
        return equipmentKind;
    }

    /**
     * Set the Equipment Kind
     *
     * @param equipmentKind the Equipment Kind
     */
    public void setEquipmentKind(String equipmentKind) {
        this.equipmentKind = equipmentKind;
    }

    /**
     * Get the Equipment Issue
     *
     * @return the equipment issue
     */
    public String getIssue() {
        return issue;
    }

    /**
     * Set the Equipment Issue
     *
     * @param issue the Equipment issue
     */
    public void setIssue(String issue) {
        this.issue = issue;
    }

    /**
     * Get the customer
     *
     * @return the customer
     */
    public Customer getCustomer() {
        return customer;
    }

    /**
     * Set the customer
     *
     * @param customer the customer
     */
    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    /**
     * Get the technical
     *
     * @return the technical
     */
    public User getTechnical() {
        return technical;
    }

    /**
     * Set the technical
     *
     * @param technical the technical
     */
    public void setTechnical(User technical) {
        this.technical = technical;
    }

    /**
     * Get the ticket status
     *
     * @return the ticket status
     */
    public int getStatus() {
        return status;
    }

    /**
     * Set the ticket status
     *
     * @param status the status
     */
    public void setStatus(int status)  {
        this.status = status;
    }

    /**
     * Gets created at.
     *
     * @return the created at
     */
    public Date getCreatedAt() {
        return createdAt;
    }

    /**
     * Sets created at.
     *
     * @param createdAt the created at
     */
    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

}